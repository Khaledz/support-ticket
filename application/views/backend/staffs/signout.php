<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
	
</div>
</div>
</div>
