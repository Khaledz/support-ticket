<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

	<?php if(isset($message)): ?>
		<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
	<?php endif; ?>

	<form class="form-horizontal" method="POST" action="">
		<div class="col-md-12">
			<div class="form-group">
				<input type="text" class="form-control form-control-solid" placeholder="Department Name" name="name" value="<?php echo set_value('name'); ?>">
			</div>

		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Create</button>
			<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
		</div>
	</form>

	</div>
</div>
</div>