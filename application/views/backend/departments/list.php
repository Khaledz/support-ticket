<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

	<?php if(!$departments): ?>
		<div class="alert alert-danger">There is no departments at this moment.</div>
	<?php else: ?>

	<table class="table table-hover">
	<thead>
	<tr>
		<th>
			 #
		</th>
		<th>
			 Department Name
		</th>
		<th>
			 Delete
		</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($departments as $department): ?>
		<tr>
		<td>
			 <?php echo $department['id']; ?>
		</td>
		<td>
			 <?php echo $department['name']; ?>
		</td>
		<td>
			 <a data-toggle="confirmation" data-original-title="Are you sure ?" title="" class="delete" href="<?php echo base_url(); ?>backend/departments/delete/<?php echo $department['id']; ?>">
		Delete </a>
		</td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>
	</tbody>
	</table>


	</div>
</div>
</div>