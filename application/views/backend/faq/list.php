<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div data-rail-color="yellow" data-handle-color="#a1b2bd">

	<?php if(!$faq): ?>
		<div class="alert alert-danger">There is no faq at this moment.</div>
	<?php else: ?>

	<div class="tab-content">
		<div id="tab_1" class="tab-pane active">
			<div id="accordion1" class="panel-group">
			<?php $i = 1; ?>
			<?php foreach($faq as $f): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>" href="#accordion1_<?php echo $i; ?>">
						<?php echo $i .'. '. $f['question']; ?>? </a>
						</h4>
					</div>
					<div id="accordion1_<?php echo $i; ?>" class="panel-collapse collapse">
						<div class="panel-body">
							 <?php echo $f['answer']; ?>. <a href="<?php echo base_url(); ?>backend/faq/delete/<?php echo $f['id']; ?>">Delete</a>
						</div>
					</div>
				</div>
				<?php $i++; ?>
			<?php endforeach; ?>
			</div>
		</div>
	</div>

<?php endif; ?>


	</div>
</div>
</div>