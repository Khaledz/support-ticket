<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar" style="width: 250px;">
	<!-- PORTLET MAIN -->
	<div class="portlet light profile-sidebar-portlet">
		<!-- SIDEBAR USERPIC -->
		<div class="profile-userpic">
			<img src="<?php echo base_url(); ?>assets/profile.png" class="img-responsive" alt="">
		</div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="profile-usertitle">
			<div class="profile-usertitle-name">
				 <?php echo $staff[0]['username']; ?>
			</div>
			<div class="profile-usertitle-job">
				 <?php echo $department->select('name','id', $staff[0]['departmentid']); ?>
			</div>
			<div class="profile-usertitle-job">
				 <i class="fa fa-star">&nbsp;</i><?php echo $rate->select('points','staffid', $staff[0]['id']); ?>
			</div>
			<br>
		</div>
		<!-- END SIDEBAR USER TITLE -->
	</div>
	<!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->

<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PORTLET -->
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption caption-md">
						<i class="icon-bar-chart theme-font hide"></i>
						<span class="caption-subject font-blue-madison bold uppercase">List of Tickets</span>
					</div>
				</div>
				<div class="portlet-body">
				<div class="table-scrollable table-scrollable-borderless">
					<?php if(!$tickets): ?>
						<div class="alert alert-danger">There is no tickets at this moment.</div>
					<?php else: ?>
						<table class="table table-hover table-light">
						<thead>
						<tr class="uppercase">
							<th>
								 #
							</th>
							<th>
								 Title
							</th>
							<th>
								 Date
							</th>
							<th>
								 Status
							</th>
							<th>
								 Priority
							</th>
							<th>
								 Handled By
							</th>
							<th>
								 View
							</th>
							<th>
								 Delete
							</th>
						</tr>
						</thead>
						<?php foreach($tickets as $ticket): ?>
						<tr>
							<td>
								 <?php echo $ticket['id']; ?>
							</td>
							<td>
								 <?php echo $ticket['title']; ?>
							</td>
							<td>
								 <?php echo $ticket['date']; ?>
							</td>
							<td>
								<?php echo $ticket['status']; ?>
							</td>
							<td>
								<?php if($ticket['priority'] == 1): ?>
									<span class="label label label-info">
										Normal 
									</span>
								<?php elseif($ticket['priority'] == 2): ?>
									<span class="label label label-warning">
										Medium
									</span>			
								<?php elseif($ticket['priority'] == 3): ?>
									<span class="label label label-danger">
										Very Important
								    </span>
								<?php endif; ?>
							</td>
							<td>
								<?php if($staffs->select('username','id',$handle->select('staffid','ticketid',$ticket['id']))) echo $staffs->select('username','id',$handle->select('staffid','ticketid',$ticket['id'])); else echo 'None'; ?>
							</td>
							<td>
								 <a href="<?php echo base_url(); ?>backend/tickets/view/<?php echo $ticket['id']; ?>">View</a>
							</td>
							<td>
								 <a data-toggle="confirmation" data-original-title="Are you sure ?" title="" class="delete" href="<?php echo base_url(); ?>backend/tickets/delete/<?php echo $ticket['id']; ?>">
							Delete </a>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
						</tr>
						</table>
					</div>
				</div>
			</div>
			<!-- END PORTLET -->
		</div>
		
		</div>
	</div>


