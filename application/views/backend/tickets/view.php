<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $ticket['title']; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
	<?php if($ticket['status'] == 'Closed'): ?>
		<div class="alert alert-danger">This ticket is closed.</div>
	<?php else: ?>
	<!-- BEGIN PAGE CONTENT-->
	<div class="timeline-body">
		<div class="timeline-body-head">
			<div class="timeline-body-head-caption">
				<a href="#" class="timeline-body-title font-blue-madison"><?php echo $user->select('username','id',$ticket['userID']); ?></a>
				<span class="timeline-body-time font-grey-cascade">Posted at <?php echo $ticket['time']; ?></span>
			</div>
		</div>
		<div class="timeline-body-content">
			<span class="font-grey-cascade">
			<?php echo $ticket['desc']; ?></span>
		</div>
	</div>

	<br>
	<?php if(!$comments): ?>
		<div class="alert alert-danger">There is no replies at this moment.</div>
	<?php else: ?>

		<!-- TIMELINE ITEM -->
		<div class="timeline-item">
		<?php foreach($comments as $comment): ?>
			<div class="timeline-body">
				<div class="timeline-body-head">
					<div class="timeline-body-head-caption">
						<a href="#" class="timeline-body-title font-blue-madison"><?php echo $user->select('username','id',$comment['userID']);  echo $staff->select('username','id',$comment['staffID']);  ?></a>
						<span class="timeline-body-time font-grey-cascade">Replied at <?php echo $comment['time']; ?></span>
					</div>
				</div>
				<div class="timeline-body-content">
					<span class="font-grey-cascade">
					<?php echo $comment['text']; ?></span>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
			<!-- END TIMELINE ITEM -->
	<!-- END PAGE CONTENT-->
	<?php echo $pagination; ?>
	<hr>
	<h4><center>Submit a Reply</center></h4>

	<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>backend/comments/create">
		<div class="col-md-12">
			<div class="form-group">
				<textarea class="wysihtml5 form-control form-control-solid" rows="6" placeholder="Type reply here .." name="comment"><?php echo set_value('comment'); ?></textarea>	
			</div>
		</div>

		<input type="hidden" name="ticketID" value="<?php echo $ticketID; ?>">

		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Submit</button>
			<a href="" class="btn btn-danger uppercase"><font color="white">Cancel</font></a>
			<input class="btn btn-info uppercase" id="closeTicketBtn" value="Close Ticket"></input>
			<input class="btn btn-warning uppercase" id="transferTicket" value="Transfer Ticket"></input>
		</div>
	</form>

<?php endif; ?>

<!-- Transfer Ticket -->
<div id="transferDailog" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Close Ticket</h4>
			</div>
			<div class="modal-body">
				<div class="message"></div>
				<div class="alert alert-info">Select the the new department for this ticket.</div>
				<div class="form-group">
					<select class="form-control form-control-solid" id="ticketDepartment">
						<option class value="">Department</option>
					</select>
				</div>

					<div class="form-actions">
						<button type="submit" id="yesTransfer" class="btn btn-success uppercase">Transfer</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					<input type="hidden" id="ticketID" value="<?php echo $ticketID; ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->	

	</div>
</div>
</div>

<!-- Close Ticket -->
<div id="closeTicket" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Close Ticket</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>
				<div class="alert alert-warning">Are you sure you want to close this ticket?</div>
					<div class="form-actions">
						<button type="submit" id="yes" class="btn btn-success uppercase">Yes</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">No</font></a>	
					<input type="hidden" id="ticketID" value="<?php echo $ticketID; ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->	



<script>

$(function() {

$('#closeTicket').hide();
$('#transferDailog').hide();

$('#closeTicketBtn').click(function(){

	var window = $("#closeTicket");
	window.css("left", 80);
	window.css("top", 150);

	window.fadeIn(500);

	$('#yes').click(function(){

		var ticketID = $('#ticketID').val();

		$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/close',
		data:{ticketID:ticketID},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();
		
		}
		});

	});

});

$('#transferTicket').click(function(){

	var window = $("#transferDailog");
	window.css("left", 80);
	window.css("top", 150);

	window.fadeIn(500);

	$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/listDepartmentsByAjax',
		data:{},
		type: 'POST',
		success: function(data){
    	
    	$('#ticketDepartment').append(data);

    	}

	});
});

$('#yesTransfer').click(function(){

	var departmentID = $('#ticketDepartment').val();
	var ticketID = $('#ticketID').val();

	$.ajax({
		url: '<?php echo base_url();?>backend/tickets/transfer',
		data:{ticketID:ticketID, departmentID:departmentID},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();

			if(data == '<div class="alert alert-success">The ticket has been transfered successfully.</div>')
			{
				setTimeout(function(){
					location.reload();
					window.location.replace('<?php echo base_url(); ?>backend/dashboard');
				}, 1000);
			}
		
		}
	});

});

//clear message
	$('.close').click(function(){
		$('#closeTicket').hide(500);
		$('#transferDailog').hide(500);
	});

});

</script>