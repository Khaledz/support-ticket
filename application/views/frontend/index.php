<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"> Home Page</span>
		</div>
	</div>

<div class="portlet-body">
<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

<h3><center>Welcome visitor to our support ticket system, please use the following options below:</center></h3>

<br>

<div class="col-lg-333 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat blue-madison">
		<div class="visual">
			<i class="fa fa-users"></i>
		</div>
		<div class="details">
			<div class="number">
				 	 &nbsp;	</div>
			<div class="desc">
				 Log In
			</div>
		</div>
		<a class="more" href="#" id="logInBtn">
		Click Here <i class="m-icon-swapright m-icon-white"></i>
		</a>
	</div>
</div>
<div class="col-lg-333 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat purple-plum">
		<div class="visual">
			<i class="fa fa-user"></i>
		</div>
		<div class="details">
			<div class="number">
				 	 &nbsp;	</div>
			<div class="desc">
				 Sign Up
			</div>
		</div>
		<a class="more" href="#" id="createUserBtn">
		Click Here <i class="m-icon-swapright m-icon-white"></i>
		</a>
	</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat red-intense">
		<div class="visual">
			<i class="fa fa-sitemap"></i>
		</div>
		<div class="details">
			<div class="number">
				 	 &nbsp;	</div>
			<div class="desc">
				 Check a Ticket
			</div>
		</div>
		<a class="more" href="#" id="checkTicketBtn">
		Click Here <i class="m-icon-swapright m-icon-white"></i>
		</a>
	</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat green-haze">
		<div class="visual">
			<i class="fa fa-table"></i>
		</div>
		<div class="details">
			<div class="number">
				 	 &nbsp;	</div>
			<div class="desc">
				 Create a Ticket
			</div>
		</div>
		<a class="more" href="#" id="createTicketBtn">
		Click Here <i class="m-icon-swapright m-icon-white"></i>
		</a>
	</div>
</div>

<!-- Log In -->
<div id="logIn" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="currBtnCloseUp" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Log In</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" action="" method="POST">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="Lusername" placeholder="Username" name="username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control form-control-solid" id="Luserpass" placeholder="Passwrod" name="userpass">
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success uppercase" id="logInSubmit">Log In</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->

<!-- Create User -->
<div id="createUser" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="currBtnCloseUp" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Create a User</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" action="" method="POST">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="Cusername" placeholder="Username" name="username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control form-control-solid" id="Cuserpass" placeholder="Passwrod" name="userpass">
						</div>
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="email" placeholder="E-mail" name="email">
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success uppercase" id="createUserSubmit">Create User</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->

<!-- Check Ticket -->
<div id="checkTicket" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="currBtnCloseUp" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Check a Ticket</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" action="" method="POST">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" placeholder="E-mail" name="name" id="cemail">
						</div>
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" placeholder="Ticket Number" name="name" id="cticketID">
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" id="checkTicketSubmit" class="btn btn-success uppercase">Go!</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->
		
<!-- Create Ticket -->
<div id="createTicket" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Create a Ticket</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" method="POST">
					<div class="col-md-12">
					<div class="form-group">
							<input type="text" class="form-control form-control-solid" placeholder="E-mail" id="ticketEmail">
						</div>
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" placeholder="Ticket Name" id="ticketName">
						</div>
						<div class="form-group">
							<textarea class="form-control form-control-solid" placeholder="Ticket Description" id="ticketDesc"></textarea>	
						</div>
						<div class="form-group">
							<select class="form-control form-control-solid" id="ticketPriority">
								<option value="">Priority</option>
								<option value="1">Normal</option>
								<option value="2">Medium</option>
								<option value="3">Very Important</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-control form-control-solid" id="ticketDepartment">
								<option class value="">Department</option>
							</select>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" id="createTicketSubmit" class="btn btn-success uppercase">Create</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->		

</div>
</div>
<br>
</div>
<!-- END Portlet PORTLET-->

<script>

$(function() {

	$('#logIn').hide();
	$('#createUser').hide();
	$('#checkTicket').hide();
	$('#createTicket').hide();

	$('.form').submit(function(e){
		e.preventDefault();    			
	});

	//clear message
	$('.close').click(function(){
		$('.message').hide();
		$('#logIn').hide(500);
		$('#createUser').hide(500);
		$('#checkTicket').hide(500);
		$('#createTicket').hide(500);
	});

	$('#logInBtn').click(function(){

		var window = $("#logIn");
    	window.css("left", 90);
    	window.css("top", 150);

    	window.fadeIn(500);
	});

	$('#logInSubmit').click(function(){
		var username = $('#Lusername').val();
		var userpass = $('#Luserpass').val();

    	$.ajax({
		url: '<?php echo base_url();?>frontend/users/login',
		data:{username:username, userpass:userpass},
		type: 'POST',
		success: function(data){

			$('.message').html(data);
			$('.message').show();

			if(data == '<div class="alert alert-success">You have logged in successfully.</div>')
			{
				setTimeout(function(){
				location.reload();
				window.location.replace('<?php echo base_url(); ?>frontend/tickets/index');
				}, 1000);
			}
		}
		});
	});

	$('#createUserBtn').click(function(){

		var window = $("#createUser");
    	window.css("left", 90);
    	window.css("top", 150);

    	window.fadeIn(500);
	});

	$('#createUserSubmit').click(function(){
		var username = $('#Cusername').val();
		var userpass = $('#Cuserpass').val();
		var email    = $('#email').val();

    	$.ajax({
		url: '<?php echo base_url();?>frontend/users/create',
		data:{username:username, userpass:userpass, email:email},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();

			if(data == '<div class="alert alert-success">You have added user successfully.</div>')
			{
				$('#createUser').hide(1300);
				$('.message').hide(1300);
				var window = $("#logIn");
		    	window.css("left", 90);
		    	window.css("top", 150);

		    	window.fadeIn(800);
			}
			
		}
		});
	});

	$('#checkTicketBtn').click(function(){

		var window = $("#checkTicket");
    	window.css("left", 80);
    	window.css("top", 150);

    	window.fadeIn(500);

	});

	$('#checkTicketSubmit').click(function(){
		var email    = $('#cemail').val();
		var ticketID = $('#cticketID').val();

    	$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/check',
		data:{email:email, ticketID:ticketID},
		type: 'POST',
		success: function(data){
			
			if(data == '0')
			{
				$('.message').html('<div class="alert alert-danger">There is no ticket with this information.</div>');
				$('.message').show();
			}
			else
			{
				$('.message').hide();
				setTimeout(function(){
					location.reload();
					window.location.replace('<?php echo base_url(); ?>frontend/tickets/view/'+data+'');
				}, 1000);
			}
			
		}
		});
	});

	$('#createTicketBtn').click(function(){

		var window = $("#createTicket");
    	window.css("left", 80);
    	window.css("top", 150);

    	window.fadeIn(500);

    	$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/listDepartmentsByAjax',
		data:{},
		type: 'POST',
		success: function(data){
    	
    	$('#ticketDepartment').append(data);

    	}

	});

    });

	$('#createTicketSubmit').click(function(){
		var email       = $('#ticketEmail').val();
		var name       = $('#ticketName').val();
		var desc       = $('#ticketDesc').val();
		var priority   = $('#ticketPriority').val();
		var department = $('#ticketDepartment').val();

    	$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/createByAjax',
		data:{email:email, name:name, desc:desc, priority:priority, department:department},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();
		
		}
		});
	});

});

</script>