<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

	<?php if(!$tickets): ?>
		<div class="alert alert-danger">There is no tickets at this moment.</div>
	<?php else: ?>
	<form action="" method="POST">
		<center>
			<h4>Filter By:</h4>
			<input type="submit" class="btn btn-circle btn-danger" name="date" value="Date"></input> &nbsp;&nbsp;
			<input type="submit" class="btn btn-circle btn-primary" name="priority" value="Priority"></input> &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" class="btn btn-circle btn-success" name="status" value="Status"></input> 
		</center>
	</form>

	<br>

	<table class="table table-hover">
	<thead>
	<tr>
		<th>
			 #
		</th>
		<th>
			 Title
		</th>
		<th>
			 Department
		</th>
		<th>
			 Date
		</th>
		<th>
			 Handle By
		</th>
		<th>
			 Status
		</th>
		<th>
			 Priority
		</th>
		<th>
			 View
		</th>
		<th>
			 Delete
		</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($tickets as $ticket): ?>
		<tr>
		<td>
			 <?php echo $ticket['id']; ?>
		</td>
		<td>
			 <?php echo $ticket['title']; ?>
		</td>
		<td>
			 <?php echo $department->select('name', 'id', $ticket['departmentID']); ?>
		</td>
		<td>
			 <?php echo $ticket['date']; ?>
		</td>
		<td>
			<?php $staffID = $handle->handleBy($ticket['id']); ?>

			<?php if(!$staffID): ?>
				<span class="label label label-info">
					None 
				</span>
			<?php else: ?> 
				<span class="label label label-success">
					<?php  $staffID = $staffID['staffid']; echo $staff->select('username','id',$staffID); ?>
				</span>
			<?php endif; ?>
		</td>
		<td>
			<?php echo $ticket['status']; ?>
		</td>
		<td>
			<?php if($ticket['priority'] == 1): ?>
				<span class="label label label-info">
					Normal 
				</span>
			<?php elseif($ticket['priority'] == 2): ?>
				<span class="label label label-warning">
					Medium
				</span>			
			<?php elseif($ticket['priority'] == 3): ?>
				<span class="label label label-danger">
					Very Important
			    </span>
			<?php endif; ?>
		</td>
		<td>
			 <a href="<?php echo base_url(); ?>frontend/tickets/view/<?php echo $ticket['id']; ?>">View</a>
		</td>
		<td>
			 <a data-toggle="confirmation" data-original-title="Are you sure ?" title="" class="delete" href="<?php echo base_url(); ?>frontend/tickets/delete/<?php echo $ticket['id']; ?>">
		Delete </a>
		</td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>
	</tbody>
	</table>


	</div>
</div>
</div>