<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $ticket['title']; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
	<?php if($ticket['status'] == 'Closed'): ?>
		<div class="alert alert-danger">This ticket is closed.</div>
	<?php else: ?>
	<!-- BEGIN PAGE CONTENT-->
	<div class="timeline-body">
		<div class="timeline-body-head">
			<div class="timeline-body-head-caption">
				<a href="#" class="timeline-body-title font-blue-madison"><?php echo $client['username']; ?></a>
				<span class="timeline-body-time font-grey-cascade">Posted at <?php echo $ticket['time']; ?></span>
			</div>
		</div>
		<div class="timeline-body-content">
			<span class="font-grey-cascade">
			<?php echo $ticket['desc']; ?></span>
		</div>
		<br>
		<?php if($ticket['image']): ?>
			<h5>Attachment:</h5>
			<img src="<?php echo base_url(); ?>assets/images/<?php echo $ticket['image']; ?>" width="350" height="300">
		<?php endif; ?>
	</div>

	<br>
	<?php if(!$comments): ?>
		<div class="alert alert-danger">There is no replies at this moment.</div>
	<?php else: ?>

		<!-- TIMELINE ITEM -->
		<div class="timeline-item">
		<?php foreach($comments as $comment): ?>
			<div class="timeline-body">
				<div class="timeline-body-head">
					<div class="timeline-body-head-caption">
						<a href="#" class="timeline-body-title font-blue-madison"><?php echo $user->select('username','id',$comment['userID']);  echo $staff->select('username','id',$comment['staffID']);  ?></a>
						<span class="timeline-body-time font-grey-cascade">Replied at <?php echo $comment['time']; ?></span>
					</div>
				</div>
				<div class="timeline-body-content">
					<span class="font-grey-cascade">
					<?php echo $comment['text']; ?></span>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
			<!-- END TIMELINE ITEM -->
	<!-- END PAGE CONTENT-->
	<?php echo $pagination; ?>
	<hr>
	<h4><center>Submit a Reply</center></h4>

	<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>frontend/comments/create">
		<div class="col-md-12">
			<div class="form-group">
				<textarea class="wysihtml5 form-control form-control-solid" rows="6" placeholder="Type reply here .." name="comment"><?php echo set_value('comment'); ?></textarea>	
			</div>
		</div>

		<input type="hidden" name="ticketID" value="<?php echo $ticketID; ?>">

		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Submit</button>
			<a href="" class="btn btn-danger uppercase"><font color="white">Cancel</font></a>
			<input class="btn btn-info uppercase" id="closeTicketBtn" value="Close Ticket"></input>
			<?php if($handle->Select('id','ticketid',$ticketID))
			{
				$staffID = $handle->select('staffid','ticketid',$ticketID);
				echo '<input type="hidden" id="staffID" value="'.$staffID.'"/>';
				echo '<input class="btn btn-info uppercase" id="rateBtn" value="Rate Staff"></input>';
			}
			?>
		</div>
	</form>

<?php endif; ?>

<!-- Rate -->
<div id="rateDailog" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Rate Staff</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>
				<div class="form-group">
				<select class="form-control form-control-solid" id="rateValue" name="rateValue">
					<option class='rateValue' value="1">1</option>
					<option class='rateValue' value="2">2</option>
					<option class='rateValue' value="3">3</option>
					<option class='rateValue' value="4">4</option>
					<option class='rateValue' value="5">5</option>
				</select>
				</div>

					<div class="form-actions">
						<button type="submit" id="doRate" class="btn btn-success uppercase">Rate</button>
					<input type="hidden" id="ticketID" value="<?php echo $ticketID; ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->				

	</div>
</div>
</div>

<!-- Create Ticket -->
<div id="closeTicket" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Close Ticket</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>
				<div class="alert alert-warning">Are you sure you want to close this ticket?</div>
					<div class="form-actions">
						<button type="submit" id="yes" class="btn btn-success uppercase">Yes</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">No</font></a>	
					<input type="hidden" id="ticketID" value="<?php echo $ticketID; ?>">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->



<script>

$(function() {

$('#closeTicket').hide();

$('#closeTicketBtn').click(function(){

	var window = $("#closeTicket");
	window.css("left", 80);
	window.css("top", 150);

	window.fadeIn(500);

	$('#yes').click(function(){

		var ticketID = $('#ticketID').val();

		$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/close',
		data:{ticketID:ticketID},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();
		
		}
		});

	});

});

$('#rateBtn').click(function(){

	var window = $("#rateDailog");
	window.css("left", 80);
	window.css("top", 150);

	window.fadeIn(500);

		var staffID = $('#staffID').val();
		$('#rateValue').change(function(){
			var value = $(this).attr('value');

			$('#doRate').click(function(){
		$.ajax({
		url: '<?php echo base_url();?>frontend/tickets/rate',
		data:{staffID:staffID, value:value},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();
		
		}
		});
	});
		});

	

});

//clear message
$('.close').click(function(){
	$('#rateDailog').hide(500);
	$('#closeTicket').hide(500);
});


});

</script>