<!-- BEGIN Portlet PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-speech"></i>
			<span class="caption-subject bold uppercase"><?php echo $title; ?></span>
		</div>
	</div>

<div class="portlet-body">
	<div class="scroller" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">

	<?php if(isset($message)): ?>
		<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
	<?php endif; ?>

	<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
		<div class="col-md-12">
			<div class="form-group">
				<input type="text" class="form-control form-control-solid" placeholder="Ticket Name" name="name" value="<?php echo set_value('name'); ?>">
			</div>
			<div class="form-group">
				<textarea class="wysihtml5 form-control form-control-solid" rows="6" placeholder="Ticket Description" name="desc"><?php echo set_value('desc'); ?></textarea>	
			</div>
			<div class="form-group">
				<select class="form-control form-control-solid" name="priority">
					<option value="">Priority</option>
					<option value="1">Normal</option>
					<option value="2">Medium</option>
					<option value="3">Very Important</option>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control form-control-solid" name="department">
					<option class value="">Department</option>
					<?php foreach($dpeartments as $department): ?>
						<option value="<?php echo $department['id']; ?>" <?php echo set_select('department', $department['id']); ?>><?php echo $department['name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				<span>Upload image (optional)</span>
				<input type="file" name="userfile"/>
			</div>
		</div>

		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase">Create</button>
			<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
		</div>
	</form>

	</div>
</div>
</div>