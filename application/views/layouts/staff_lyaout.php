<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php echo isset($title) ? 'Support Ticket - '.$title.'' : 'Support Ticket' ; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<script src="<?php echo base_url(); ?>assets/jquery-1.11.2.min.js"></script>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url(); ?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo base_url(); ?>assets/admin/layout4/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="index.html">
			<img src="assets/admin/layout4/img/logo-light.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN PAGE TOP -->
		<div class="page-top">

		<?php if($this->session->userdata('staffdata')): $session = $this->session->userdata('staffdata');?>
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<li class="separator hide">
				</li>
				
				<li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-user"></i>
						<span class="badge badge-success">
							<?php echo 'Welcome '.$session['username']; ?> 
						</span>
					&nbsp;&nbsp;&nbsp;&nbsp;</a>
				</li>
				<li class="separator hide">
				</li>
				<li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
					<a href="<?php echo base_url(); ?>backend/staffs/logout" class="dropdown-toggle"  data-hover="dropdown" data-close-others="true">
					<i class="fa fa-sign-out"></i>&nbsp;&nbsp;&nbsp;&nbsp;
					</a>
				</li>					
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->

		
		<?php endif; ?>

		</div>
		<!-- END PAGE TOP -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<?php if(!$this->session->userdata('staffdata')): ?>

			<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<li class="start ">
					<a href="<?php echo base_url(); ?>backend/dashboard">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
				</li>
			</ul>

			<?php else: ?>

			<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<li class="start ">
					<a href="<?php echo base_url(); ?>backend/dashboard">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
					<i class="icon-grid"></i>
					<span class="title">Departments</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>backend/departments/index">
							<i class="icon-grid"></i>
							List Departments</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>backend/Departments/create">
							<i class="icon-plus"></i>
							Create Department</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="icon-question"></i>
					<span class="title">FAQ</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>backend/faq/index">
							<i class="icon-question"></i>
							List FAQ</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>backend/faq/create">
							<i class="icon-plus"></i>
							Create FAQ</a>
						</li>
					</ul>
				</li>
			</ul>
			<?php endif; ?>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php 
					if(isset($view) && isset($data))
					{
						$this->load->view($view, $data);
					}
					elseif(isset($view) && !isset($data))
					{
						$this->load->view($view);
					}
					else
					{
						echo 'Cannot load content';
					}		
					?>

				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
<!-- Log In -->
<div id="logIn" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="currBtnCloseUp" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Log In</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" action="" method="POST">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="Lusername" placeholder="Username" name="username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control form-control-solid" id="Luserpass" placeholder="Passwrod" name="userpass">
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success uppercase" id="logInSubmitTop">Log In</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->

<!-- Create User -->
<div id="createUser" class="modal  bs-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="currBtnCloseUp" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Create a User</h4>
			</div>
			<div class="modal-body">

				<div class="message"></div>

				<form class="form form-horizontal" role="form" action="" method="POST">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="Cusername" placeholder="Username" name="username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control form-control-solid" id="Cuserpass" placeholder="Passwrod" name="userpass">
						</div>
						<div class="form-group">
							<input type="text" class="form-control form-control-solid" id="email" placeholder="E-mail" name="email">
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success uppercase" id="createUserSubmitTop">Create User</button>
						<a href=""class="btn btn-danger uppercase"><font color="white">Cancel</font></a>	
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- End -->

		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2015 &copy; Support Ticket System.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<script>
      jQuery(document).ready(function() {    
         Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
      });
   </script>

<script>

$(function() {

	$('#logIn').hide();
	$('#createUser').hide();

	$('.form').submit(function(e){
		e.preventDefault();    			
	});

	//clear message
	$('.close').click(function(){
		$('.message').hide();
		$('#logIn').hide(500);
		$('#createUser').hide(500);
		location.reload();
	});

	$('#logInTop').click(function(){

		var window = $("#logIn");
    	window.css("left", 90);
    	window.css("top", 150);

    	window.fadeIn(500);
	});

	$('#logInSubmitTop').click(function(){
		var username = $('#Lusername').val();
		var userpass = $('#Luserpass').val();

    	$.ajax({
		url: '<?php echo base_url();?>backend/users/login',
		data:{username:username, userpass:userpass},
		type: 'POST',
		success: function(data){

			$('.message').html(data);
			$('.message').show();
			
		}
		});
	});

	$('#createUserTop').click(function(){

		var window = $("#createUser");
    	window.css("left", 90);
    	window.css("top", 150);

    	window.fadeIn(500);
	});

	$('#createUserSubmitTop').click(function(){
		var username = $('#Cusername').val();
		var userpass = $('#Cuserpass').val();
		var email    = $('#email').val();

    	$.ajax({
		url: '<?php echo base_url();?>backend/users/create',
		data:{username:username, userpass:userpass, email:email},
		type: 'POST',
		success: function(data){
			
			$('.message').html(data);
			$('.message').show();
			
		}
		});
	});


});

</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

