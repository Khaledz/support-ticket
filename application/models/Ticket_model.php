<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');

        $this->table = 'ticket';
    }

    function getRowsById($id)
    {
    	$this->db->where('userID', $id);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
			return $query->result_array();
		else
			return false;
    }

    function getTicketsByDepartment($id)
    {
        $this->db->where('departmentID', $id);
        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;
    }

    function check($email, $ticketID)
    {
        $query = $this->db->query('SELECT id FROM ticket WHERE email = "'.$email.'" AND id = '.$ticketID.'');

        if($query->num_rows() > 0)
        {
            return $query->row()->id;
        }
        return false;
    }

    function getLastID()
    {
        $this->db->select('id');
        $this->db->order_by('id','desc');
        $query = $this->db->get($this->table);

        return $query->row()->id;
    }

    function canAccess($userID, $ticketID)
    {
        $this->db->where('userID', $userID);
        $this->db->where('id', $ticketID);
        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
        {
            return true;
        }
        return false;
    }
}