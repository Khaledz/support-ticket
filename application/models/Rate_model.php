<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rate_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'rate';

        $this->load->model('rate_model');
    }

    function rate($staffID, $value)
    {
      if(!$this->isExist($staffID))
      {
        $data = array('staffid' => $staffID, 'points' => $value);
        $this->rate_model->save($data);
        return true;
      }

    	$perPoints =  $this->rate_model->select('points','staffid', $staffID);
    	$points = ($value) + ($perPoints);
    	$query = $this->db->query('update rate set points ='.$points.' WHERE staffid ='.$staffID.'');
   		if($query)
   		{
   			return true;
   		}
   		return false;
    }

    public function isExist($staffid)
    {
      $this->db->where('staffid', $staffid);
      $query = $this->db->get($this->table);
      if($query->num_rows() > 0)
        return true;
      else
        return false;
    }
}