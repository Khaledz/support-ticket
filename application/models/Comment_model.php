<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'comment';
    }

    function getCommentsByTicketID($ticketID, $start, $limit)
    {
    	$this->db->where('ticketID', $ticketID);
        $this->db->limit($start, $limit);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
			return $query->result_array();
		else
			return false;
    }

    function countComments($ticketID)
    {
        $this->db->where('ticketID', $ticketID);
        $query = $this->db->get($this->table);
            return $query->num_rows();
    }
}