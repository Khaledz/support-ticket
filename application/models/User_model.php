<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'client';
    }

	function login($username, $userpass)
	{
		$query = $this->db->get_where($this->table, array('username' => $username, 'userpass' => md5($userpass)));
		if($query->num_rows() > 0)
			return true;
		else
			return false;
	}
}