<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffhandle_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'staffhandle';
    }

    function handleBy($ticketID)
    {
    	$this->db->where('ticketid', $ticketID);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
			return $query->row_array();
		else
			return false;
    }
}