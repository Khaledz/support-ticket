<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends MY_Staff 
{
	function __construct()
	{
        	parent::__construct();

        	$this->load->model('comment_model');
                $this->load->model('staffhandle_model');
                $this->load->model('ticket_model');
	}

	public function create()
	{

        if($this->input->post())
        {
        	$data = array(
        		'text' => $this->input->post('comment'),
        		'date' => date('Y-m-d'),
        		'time' => date('H:M:S'),
        		'ticketID' => $this->input->post('ticketID'),
        		'userID' => 0,
                        'staffID' => $this->staffID
        	);

                $handleBy = array(
                        'staffid' => $this->staffID,
                        'ticketid' => $this->input->post('ticketID')
                );

        	$this->comment_model->save($data);

                if(!$this->staffhandle_model->select('id','id',$this->input->post('ticketID')))
                {
                        $this->staffhandle_model->save($handleBy);
                }

        	redirect('backend/tickets/view/'.$data['ticketID'].'');
        }

		$this->load->view($this->layout, $data);
	}
}
