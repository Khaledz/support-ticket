<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends MY_Staff 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('department_model');
	}

	public function index()
	{
        $data = array(
            'title' => 'List Departments',
            'view' => 'backend/departments/list',
            'departments' => $this->department_model->getRows(),
        );

        $this->load->view($this->layout, $data);
	}

	public function create()
	{
        $this->form_validation->set_rules('name', 'department name', 'required|strip_tags|trim');

		$data = array(
	        'title' => 'Create Department',
	        'view' => 'backend/departments/create'
        );

        $department = array(
            'name' => $this->input->post('name')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->department_model->save($department);

                $data['message'] = 'The department has been added successfully.';
                $data['class']   = 'alert alert-success';

                echo '<meta http-equiv="refresh" content="3; URL='.base_url().'backend/departments/index">';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

        $this->load->view($this->layout, $data);
	}

    function delete($id)
    {
        $this->department_model->delete($id);

        redirect('backend/departments/');
    }
	
}