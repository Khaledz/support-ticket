<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staffs extends MY_Staff 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('staff_model');
	}

	public function login()
	{
        $this->form_validation->set_rules('username', 'user name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('userpass', 'password', 'required|trim|strip_tags');

        $data = array(
            'username' => $this->input->post('username'),
            'userpass' => md5($this->input->post('userpass')),
            'staffID' => $this->staff_model->select('id','username', $this->input->post('username')),
            'departmentID' => $this->staff_model->select('departmentID','id', $this->staff_model->select('id','username', $this->input->post('username')))
        );

        if($this->form_validation->run())
        {
            if($this->staff_model->login($this->input->post('username'), $this->input->post('userpass')))
            {
            	$this->session->set_userdata('staffdata', $data);

                redirect(base_url(). 'backend/dashboard');           
            }
            else
            {
            	$data['message'] = '<div class="alert alert-danger">Username/Password is not correct.Please try again.</div>';
            }
        }

        $this->load->view('backend/login', $data);
	}

	public function logout()
	{
		$data = array(
	        'title' => 'Sign Out',
	        'view' => 'backend/staffs/signout'
        );

        if($this->session->userdata('staffdata'))
        {
        	$this->session->unset_userdata('staffdata');
            $this->session->sess_destroy();

        	$data['message'] = 'You have logged out successfully.';
        	$data['class']   = 'alert alert-success';

        }
        else
        {
            $data['message'] = 'You do not have permission to access this page.';
            $data['class']   = 'alert alert-danger';
        }

        $this->load->view($this->layout, $data);
	}
	
}