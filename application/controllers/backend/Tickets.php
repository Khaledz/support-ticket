<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends MY_Staff 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('ticket_model');
		$this->load->model('user_model');
		$this->load->model('department_model');
		$this->load->model('staffhandle_model');
		$this->load->model('staff_model');
		$this->load->model('comment_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'List tickets',
	        'view' => 'backend/tickets/index'
        );

		$this->load->view($this->layout, $data);
	}

	public function view($id)
	{
	    //pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'backend/tickets/view/'.$id.'';
        $config['total_rows'] = $this->comment_model->countComments($id);
        $config['per_page'] = 8; 
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination pagination"><li>';
        $config['full_tag_close'] = '</ul></li>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['first_link'] = 'First';

        $this->pagination->initialize($config); 

        if($this->uri->segment(5))
        {
            $page = $this->uri->segment(5);
        }
        else
        {
            $page = 0;
        }   

        $data = array(
	        'title' => 'View ticket',
	        'view' => 'backend/tickets/view',
	        'ticket' => $this->ticket_model->getRow($id),
	        'client' => $this->user_model->getRow($this->staffID),
	        'comments' => $this->comment_model->getCommentsByTicketID($id,$config['per_page'], $page),
	        'ticketID' => $id,
	        'staff' => $this->staff_model,
	        'user' => $this->user_model
	    );

        $data['pagination'] = $this->pagination->create_links();

		$this->load->view($this->layout, $data);
	}

	function delete($id)
    {
        $this->ticket_model->delete($id);

        redirect('backend/dashboard/');
    }

    function transfer()
    {
    	$data = array(
    		'id' => $this->input->post('ticketID'),
    		'departmentID' => $this->input->post('departmentID')
    	);

    	if($data)
    	{
    		$this->ticket_model->update($data);
    		echo '<div class="alert alert-success">The ticket has been transfered successfully.</div>';
    		return;
    	}

    	echo '<div class="alert alert-danger">Something went wrong, please try again.</div>';
    }
}
