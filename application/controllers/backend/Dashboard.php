<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Staff 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('staff_model');
		$this->load->model('department_model');
		$this->load->model('rate_model');
		$this->load->model('ticket_model');
		$this->load->model('staffhandle_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'Home Page',
	        'view'  => 'backend/dashboard',
	        'staff' => $this->staff_model->getRows(),
	        'department' => $this->department_model,
	        'rate' => $this->rate_model,
	        'tickets' => $this->ticket_model->getTicketsByDepartment($this->departmentID),
	        'handle' => $this->staffhandle_model,
	        'staffs' => $this->staff_model
        );

		$this->load->view($this->layout, $data);
	}
}
