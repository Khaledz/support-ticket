<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Staff 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('faq_model');
	}

	public function index()
	{
        $data = array(
            'title' => 'List Faq',
            'view' => 'backend/faq/list',
            'faq' => $this->faq_model->getRows(),
        );

        $this->load->view($this->layout, $data);
	}

	public function create()
	{
        $this->form_validation->set_rules('question', 'question', 'required|strip_tags|trim');
        $this->form_validation->set_rules('answer', 'answer', 'required|strip_tags|trim');

		$data = array(
	        'title' => 'Create FAQ',
	        'view' => 'backend/faq/create'
        );

        $faq = array(
            'question' => $this->input->post('question'),
            'answer' => $this->input->post('answer')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->faq_model->save($faq);

                $data['message'] = 'The question has been added successfully to faqs page.';
                $data['class']   = 'alert alert-success';

                echo '<meta http-equiv="refresh" content="3; URL='.base_url().'backend/faq/index">';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

        $this->load->view($this->layout, $data);
	}

    function delete($id)
    {
        $this->faq_model->delete($id);

        redirect('backend/faq/');
    }
	
}