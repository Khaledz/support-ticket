<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends MY_User 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('ticket_model');
		$this->load->model('user_model');
		$this->load->model('department_model');
		$this->load->model('staffhandle_model');
		$this->load->model('staff_model');
		$this->load->model('comment_model');
        $this->load->model('rate_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'List tickets',
	        'view' => 'frontend/tickets/list',
	        'tickets' => $this->ticket_model->getRowsById($this->userID),
	        'department' => $this->department_model,
	        'handle' => $this->staffhandle_model,
	        'staff' => $this->staff_model
        );

        //filter list
        if($this->input->post())
        {
        	$date     = $this->input->post('date');
        	$priority = $this->input->post('priority');
        	$status   = $this->input->post('status');

        	if(isset($date))
        	{
        		$this->db->where('userID', $this->userID);
        		$this->db->order_by('date','desc');
        		$data['tickets'] = $this->db->get('ticket')->result_array();
        	}
        	else if(isset($priority))
        	{
        		$this->db->where('userID', $this->userID);
        		$this->db->order_by('priority','desc');
        		$data['tickets'] = $this->db->get('ticket')->result_array();
        	}
        	else if(isset($status))
        	{
        		$this->db->where('userID', $this->userID);
        		$this->db->order_by('status','desc');
        		$data['tickets'] = $this->db->get('ticket')->result_array();
        	}
        }

		$this->load->view($this->layout, $data);
	}

	public function view($id)
	{
	    //pagination
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'frontend/tickets/view/'.$id.'';
        $config['total_rows'] = $this->comment_model->countComments($id);
        $config['per_page'] = 8; 
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination pagination"><li>';
        $config['full_tag_close'] = '</ul></li>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['first_link'] = 'First';

        $this->pagination->initialize($config); 

        if($this->uri->segment(5))
        {
            $page = $this->uri->segment(5);
        }
        else
        {
            $page = 0;
        }   

        $data = array(
	        'title' => 'View ticket',
	        'view' => 'frontend/tickets/view',
	        'ticket' => $this->ticket_model->getRow($id),
	        'client' => $this->user_model->getRow($this->userID),
	        'comments' => $this->comment_model->getCommentsByTicketID($id,$config['per_page'], $page),
	        'ticketID' => $id,
	        'user' => $this->user_model,
            'staff' => $this->staff_model,
            'handle' => $this->staffhandle_model
	    );

        $data['pagination'] = $this->pagination->create_links();

		$this->load->view($this->layout, $data);
	}

	public function create()
	{
		$this->form_validation->set_rules('name', 'ticket name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('desc', 'ticket description', 'required|strip_tags');
        $this->form_validation->set_rules('priority', 'priority', 'required|strip_tags|integer');
        $this->form_validation->set_rules('department', 'department', 'required|strip_tags|integer');

		$data = array(
	        'title' => 'Create a new ticket',
	        'view' => 'frontend/tickets/create',
	        'dpeartments' => $this->department_model->getRows()
        );

		$ticket = array(
			'title' => $this->input->post('name'),
			'desc' => $this->input->post('desc'),
			'priority' => $this->input->post('priority'),
			'departmentID' => $this->input->post('department'),
			'userID' => $this->userID,
			'status' => 'Opened',
			'date' => date('Y-m-d'),
			'time' => date('h:i:sa')
		);

        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1024';
        $config['max_width'] = '1024';
        $config['max_height'] = '1024';

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

		if($this->input->post())
		{
	        if($this->form_validation->run())
	        {
                if(!$this->upload->do_upload() && $_FILES['userfile']['error'] != 4)
                {
                    $data['message'] = $this->upload->display_errors();
                    $data['class']   = 'alert alert-danger';
                }
                else
                {
                    $data['message'] = 'The ticket has been added successfully.';
                    $data['class']   = 'alert alert-success';

                    $image = $this->upload->data();
                    $image = $image['file_name'];

                    $ticket['image'] = $image;
                    $this->ticket_model->save($ticket);

                    echo '<meta http-equiv="refresh" content="3; URL='.base_url().'frontend/tickets">';
                }
	        }
	        else
            {
            	$data['message'] = validation_errors();
            	$data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	function delete($id)
    {
        $this->ticket_model->delete($id);

        redirect('frontend/tickets/');
    }

    function check()
    {
    	$data = array(
    		'email' => $this->input->post('email'),
    		'ticketID' =>$this->input->post('ticketID')
    	);

    	$check = $this->ticket_model->check($data['email'], $data['ticketID']);

    	if($check)
    	{
    		echo $check;
    	}
    	else
    	{
    		echo '0';
    	}
    }

    function createByAjax()
    {
		   	
    	$this->form_validation->set_rules('email', 'E-mail', 'required|trim|strip_tags|valid_email|is_unique[client.useremail]');
    	$this->form_validation->set_rules('name', 'ticket name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('desc', 'ticket description', 'required|strip_tags');
        $this->form_validation->set_rules('priority', 'priority', 'required|strip_tags|integer');
        $this->form_validation->set_rules('department', 'department', 'required|strip_tags|integer');

		$ticket = array(
			'title' => $this->input->post('name'),
			'desc' => $this->input->post('desc'),
			'priority' => $this->input->post('priority'),
			'departmentID' => $this->input->post('department'),
			'userID' => 0,
			'status' => 'Opened',
			'date' => date('Y-m-d'),
			'time' => date('h:i:sa'),
			'email' => $this->input->post('email')
		);

		if($this->form_validation->run())
        {
        	if($this->ticket_model->save($ticket))
            {
            	//Send Email
				$this->load->library('email');

				$this->email->from('dev@khaledz.com', 'Supprt Ticket');
				$this->email->to($this->input->post('email')); 

				$this->email->subject('Supprt Ticket - Ticket ID: '.$this->ticket_model->getLastID().'');
				$this->email->message('This is an electronic copy of your ticket<br>You can view your ticket by entring email and ticket ID in home page.');	

				$this->email->send();

				echo $this->email->print_debugger();

            	echo '<div class="alert alert-success">You have created a ticket successfully. Ticket ID: '.$this->ticket_model->getLastID().'
            	<br>
            	A copy of ticket detail has been sent to this email: '.$this->input->post('email').'
            	</div>';
            	return;
            }
   		}

   	   echo '<div class="alert alert-danger">'.validation_errors().'</div>';
    }

    function listDepartmentsByAjax()
    {
    	$departments = $this->department_model->getRows();

        foreach($departments as $department)
        {
        	echo '<option value="'.$department['id'].'">'.$department['name'].'</option>';
        }
    }

    function close()
    {
    	$data = array(
    		'id' => $this->input->post('ticketID'),
    		'status' => 'Closed'
    	);

    	$this->ticket_model->update($data);

    	echo '<div class="alert alert-success">You have closed the ticket successfully.</div>';
    }

    function rate()
    {
        if($this->rate_model->rate($this->input->post('staffID'), $this->input->post('value')))
        {
            echo '<div class="alert alert-success">You have rated the staff successfully.</div>';
            return;
        }

        echo '<div class="alert alert-danger">something went wrong, please try again.</div>';
    }
}
