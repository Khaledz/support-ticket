<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_User 
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array(
	        'title' => 'Home Page',
	        'view' => 'frontend/index',
        );

		$this->load->view($this->layout, $data);
	}
}
