<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends MY_User 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('comment_model');
	}

	public function create()
	{
        if($this->input->post())
        {
        	$data = array(
        		'text' => $this->input->post('comment'),
        		'date' => date('Y-m-d'),
        		'time' => date('H:M:S'),
        		'ticketID' => $this->input->post('ticketID'),
        		'userID' => $this->userID,
                        'staffID' => 0
        	);

        	$this->comment_model->save($data);

        	redirect('frontend/tickets/view/'.$data['ticketID'].'');
        }

		$this->load->view($this->layout, $data);
	}
}
