<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_User 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('user_model');
	}

	public function create()
	{
        $this->form_validation->set_rules('username', 'user name', 'required|strip_tags|trim|is_unique[client.username]');
        $this->form_validation->set_rules('userpass', 'password', 'required|trim|strip_tags');
        $this->form_validation->set_rules('email', 'E-mail', 'required|trim|strip_tags|valid_email|is_unique[client.useremail]');

        $user = array(
        	'username' => $this->input->post('username'),
        	'userpass'=> md5($this->input->post('userpass')),
        	'useremail' => $this->input->post('email')
        );

        if($this->form_validation->run())
        {
            if($this->user_model->save($user))
            {
            	echo '<div class="alert alert-success">You have added user successfully.</div>';
                return;
            }
        }

        echo '<div class="alert alert-danger">'.validation_errors().'</div>';
	}

	public function login()
	{
        $this->form_validation->set_rules('username', 'user name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('userpass', 'password', 'required|trim|strip_tags');

        if($this->form_validation->run())
        {
            if($this->user_model->login($this->input->post('username'), $this->input->post('userpass')))
            {
            	echo '<div class="alert alert-success">You have logged in successfully.</div>';

            	$session = array(
            		'username' => $this->input->post('username'),
            		'userpass' => md5($this->input->post('userpass')),
                    'userID' => $this->user_model->select('id','username', $this->input->post('username'))
            	);

            	$this->session->set_userdata('userdata', $session);
            }
            else
            {
            	echo '<div class="alert alert-danger">Username/Password is not correct.Please try again.</div>';
            }
        }
	}

	public function logout()
	{
		$data = array(
	        'title' => 'Sign Out',
	        'view' => 'frontend/users/signout'
        );

        if($this->session->userdata('userdata'))
        {
        	$this->session->unset_userdata('userdata');
            $this->session->sess_destroy();

        	$data['message'] = 'You have logged out successfully.';
        	$data['class']   = 'alert alert-success';

        }
        else
        {
            $data['message'] = 'You do not have permission to access this page.';
            $data['class']   = 'alert alert-danger';
        }

        $this->load->view($this->layout, $data);
	}
	
}