<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_User 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('faq_model');
	}

	public function index()
	{
        $data = array(
            'title' => 'List Faq',
            'view' => 'frontend/faq/list',
            'faq' => $this->faq_model->getRows(),
        );

        $this->load->view($this->layout, $data);
	}
}