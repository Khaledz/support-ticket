<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Staff extends CI_Controller
{
	protected $layout;

	protected $staffID;

	protected $departmentID;

    public function __construct()
    {
       parent::__construct();

        $this->layout = 'layouts/staff_lyaout';

        if($this->session->userdata('staffdata'))
        {
            $session = $this->session->userdata('staffdata');
                $this->staffID = $session['staffID'];
                $this->departmentID = $session['departmentID'];
        }

        date_default_timezone_set("Asia/Kuala_Lumpur");
    }
} 