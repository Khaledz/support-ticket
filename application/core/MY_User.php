<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_User extends CI_Controller
{
    protected $layout;

    protected $userID;

    public function __construct()
    {
        parent::__construct();

        $this->layout = 'layouts/user_layout';

        if($this->session->userdata('userdata'))
        {
            $session = $this->session->userdata('userdata');
                $this->userID = $session['userID'];
        }

        date_default_timezone_set("Asia/Kuala_Lumpur");
    }
} 