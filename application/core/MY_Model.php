<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_model
{
    protected $table;

	function __construct()
    {
        parent::__construct();
    }

    function getRows()
    {
    	$this->db->order_by('id', 'asc');
    	$query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;
    }

	function getRow($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
			return $query->row_array();
		else
			return false;
	}

    function select($target, $where, $value)
    {
        $query = $this->db->query("SELECT {$target} FROM {$this->table} WHERE {$where} = '{$value}'");
        if($query->num_rows() > 0)
            return $query->row()->{$target};
        else
            return false;
    }

    function save($data)
    {
        $this->db->insert($this->table, $data); 
        return $this->db->insert_id();
    }

    function update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($this->table, $data); 
        	return $this->db->affected_rows();
        return false;
    }

    function delete($data)
    {
        $this->db->delete($this->table, array('id' => $data));
        return $this->db->affected_rows();
    }

    function count()
    {
        $query = $this->db->get($this->table);
            return $query->num_rows();
    }
}
